#include <SoftwareSerial.h>
SoftwareSerial serial(9,10);

int HighLen = 0;
int LowLen  = 0;
int Len_mm  = 0;
int sensor = 0;
int sensorLow = 350;
int sensorHigh = 600;
int light = 0;
int distance = 0;
const int ledPin = 3;

void setup() {
  Serial.begin(9600);
  serial.begin(9600);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
}

void loop() {
  sensor = analogRead(A0);
  if (sensor > 0) {
    light = sensor;
  }
  if (sensor >= sensorHigh){
    digitalWrite(ledPin, LOW);
  }
  else{
    digitalWrite(ledPin, HIGH);
  }
  
  serial.write(0X55);
  if (serial.available() >= 2){
    HighLen = serial.read();
    LowLen = serial.read();
    Len_mm = HighLen * 256 + LowLen;
    if ((Len_mm > 1) && (Len_mm < 10000)){
      distance = Len_mm;
    }
  }

  if (light > 0 || distance > 0) {
    Serial.print(light);
    Serial.print(" ");
    Serial.print(distance, DEC);
    Serial.println("");
  }
  delay(1000);
}
