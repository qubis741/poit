from threading import Lock
from flask import Flask, render_template, session, request, jsonify, url_for
from flask_socketio import SocketIO, emit, disconnect
import MySQLdb       
import time
import random
import ConfigParser
import serial
import re
#ser = serial.Serial('/dev/ttyUSB0', 115200)
rSer = serial.Serial('/dev/ttyUSB0', 9600)
async_mode = None

#CREATE DATABASE `auto_fckin_mobile`
#CREATE TABLE `movement` (
#	`id` INT NOT NULL AUTO_INCREMENT,
#	`data` MEDIUMTEXT NOT NULL, //VARCHAR(65535) 
#	PRIMARY KEY (`id`)
#)
#COLLATE='utf8mb4_general_ci'
#;
#SHOW CREATE TABLE `auto_fckin_mobile`.`movement`;

app = Flask(__name__)

app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
thread = None
thread_lock = Lock() 

config = ConfigParser.ConfigParser()
config.read('config.cfg')
myhost = config.get('mysqlDB', 'host')
myuser = config.get('mysqlDB', 'user')
mypasswd = config.get('mysqlDB', 'passwd')
mydb = config.get('mysqlDB', 'db')

def background_thread(args):
    db = MySQLdb.connect(host=myhost,user=myuser,passwd=mypasswd,db=mydb)
    cursor = db.cursor()
    count = 0    
    dataList = []
    serialTemp = ""
    length = 0
    lengthTemp = 0
    sV = ""
    tV = ""
    srlV = ""
    while True:
        if args:
          A = dict(args).get('A')
          sV = dict(args).get('s_value')
          tV = dict(args).get('t_value')
          srlV = dict(args).get('value')
          btnV = dict(args).get('btn_value')
        else:
          A = 1
          srlV = 'null'
          btnV = ""
        #print A
        socketio.sleep(1)
        count += 1
        n = rSer.readline()
        senzors = n.split(" ")
 #       if srlV != serialTemp or senzors[1] != lengthTemp:
 #           serialTemp = srlV
 #           lengthTemp = senzors[1]
 #           if int(senzors[1]) < 500:
 #               if sV > 1500:
 #                   sV = 1150
 #               if sV < 1200
 #                   sV = 1150
 #               if sV < 1300
 #                   sV = 1200
 #           if int(senzors[1]) < 1000:
 #               if sV > 1500:
 #                  sV = 1150 + (sV - 1500)
 #               if sV > 1300:
 #                    sV = 1100 + (sV - 1300)
 #           if int(senzors[1]) < 2000:
 #               if sV > 1500:
 #                   sV = 1100 + (sV - 1500)
 #           socketio.emit('dist_resp',
 #                     {'data': sV, namespace='/test')  
        dataDict = {
              "time": count,
              "speed": sV,
              "throttle": tV,
              "dist": int(senzors[1]),
              "light": senzors[0]
              }
        #ser.write(srlV)
        if btnV == "start":
            dataList.append(dataDict)
            print dataDict
        if btnV == "stop" and len(dataList)>0:
            btnV = "null"
            output = str(dataList).replace("[","")
            output = output.replace("]","")
            cursor.execute("INSERT INTO movement (data) VALUES (%s)",[output])
            db.commit()
            del dataList[:]
	#print count
        socketio.emit('graph_resp',
                      {'time': count, 'data': int(senzors[1])},namespace='/test')  

@app.route('/')
def index():
    return render_template('index.html', async_mode=socketio.async_mode)

@app.route('/dbdata/<int:num>', methods=['GET', 'POST'])
def dbdata(num):
  db = MySQLdb.connect(host=myhost,user=myuser,passwd=mypasswd,db=mydb)
  cursor = db.cursor()
  print num
  cursor.execute("SELECT data FROM movement WHERE id=%i" % (num))
  rv = cursor.fetchone()
  print rv
  return str(rv[0])
  
@socketio.on('my_event', namespace='/test')
def test_message(message):   
    session['receive_count'] = session.get('receive_count', 0) + 1 
    session['A'] = message['value']
    emit('my_response',
         {'data': message['value'], 'count': session['receive_count']})
 
@socketio.on('disconnect_request', namespace='/test')
def disconnect_request():
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': 'Disconnected!', 'count': session['receive_count']})
    disconnect()

@socketio.on('connect', namespace='/test')
def test_connect():
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(target=background_thread, args=session._get_current_object())
    emit('my_response', {'data': 'Connected', 'count': 0})

@socketio.on('range_event', namespace='/test')
def db_message(message):   
    session['s_value'] = message['sValue']
    session['t_value'] = message['tValue']
    session['value'] = message['value']
    emit('my_resp',
         {'val': session['s_value']})
    
@socketio.on('click_event', namespace='/test')
def db_message(message):   
    session['btn_value'] = message['value'] 

@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected', request.sid)

if __name__ == '__main__':
    socketio.run(app, host="0.0.0.0", port=80, debug=True)
